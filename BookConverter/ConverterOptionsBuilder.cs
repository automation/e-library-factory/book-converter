using Optional;

namespace BookConverter
{
    public class ConverterOptionsBuilder
    {
        private Option<string> _inputDirectoryPath;
        private Option<string> _outputDirectoryPath;
        private Option<CompressionLevel> _compressionLevel;
        
        public ConverterOptionsBuilder InputDirectory(string path)
        {
            _inputDirectoryPath = path.Some();
            return this;
        }

        public ConverterOptionsBuilder OutputDirectory(string path)
        {
            _outputDirectoryPath = path.Some();
            return this;
        }

        public ConverterOptionsBuilder CompressionLevel(CompressionLevel compressionLevel)
        {
            _compressionLevel = compressionLevel.Some();
            return this;
        }
    
        public ConverterOptions Build() 
        {
            return new ConverterOptions(_inputDirectoryPath, _outputDirectoryPath, _compressionLevel);
        }
    }
}