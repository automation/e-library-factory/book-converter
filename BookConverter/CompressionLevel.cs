namespace BookConverter
{
    public enum CompressionLevel
    {
        NoCompression = 0,
        StandartDOC = 1,
        KindleHuffdic = 2
    }
}