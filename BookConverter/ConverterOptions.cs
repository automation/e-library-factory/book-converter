using Optional;

namespace BookConverter
{
  public class ConverterOptions
    {
        public Option<string> InputDirectoryPath { get; private set; }
        public Option<string> OutputDirectoryPath {get; private set; }
        public Option<CompressionLevel> CompressionLevel { get; private set; }

        public ConverterOptions(Option<string> inputDirectoryPath, Option<string> outputDirectoryPath, Option<CompressionLevel> compressionLevel)
        {
            InputDirectoryPath = inputDirectoryPath;
            OutputDirectoryPath = outputDirectoryPath;
            CompressionLevel = compressionLevel;
        }
    }
}