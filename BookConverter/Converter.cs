using System;

namespace BookConverter
{
    public class Converter
    {
        public static Converter Load(Func<ConverterOptionsBuilder, ConverterOptionsBuilder> p)
        {
            var a = p.Invoke(new ConverterOptionsBuilder());
            return new Converter();
        }

        public void Convert() 
        {

        }
    }
}