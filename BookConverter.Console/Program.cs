﻿using System;
namespace BookConverter.console
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            BookConverter.Converter
                .Load(options =>
                    options
                        .InputDirectory("")
                        .OutputDirectory("")
                        .CompressionLevel(CompressionLevel.KindleHuffdic)
                    )
                .Convert();
        }
    }
}
